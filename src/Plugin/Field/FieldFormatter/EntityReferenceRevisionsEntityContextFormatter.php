<?php

namespace Drupal\entity_reference_revisions_context\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsEntityFormatter;

/**
 * Plugin implementation of the 'entity reference rendered' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_revisions_entity_view_context",
 *   label = @Translation("Rendered entity with context"),
 *   description = @Translation("Display the referenced entity with added contextual data attributes."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class EntityReferenceRevisionsEntityContextFormatter extends EntityReferenceRevisionsEntityFormatter implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($elements as $delta => $element) {
      $this->addPreviousElementContext($elements, $delta);
      $this->addNextElementContext($elements, $delta);
      $this->addPositionContext($elements, $delta);
    }

    return $elements;
  }

  /**
   * Add data attributes for the previous paragraph type.
   *
   * Alternatively add first if there is no previous paragraph.
   */
  protected function addPreviousElementContext(&$elements, $delta) {
    if ($entity_bundle = $this->findElementBundle($elements, $delta - 1)) {
      $elements[$delta]['#attributes']['data-entity-context-prev'] = $entity_bundle;
      $this->addGroupElementContext($elements, $delta, $entity_bundle);
    }
    else {
      $elements[$delta]['#attributes']['data-entity-context-first'] = TRUE;
      $this->addGroupElementContext($elements, $delta);
    }
  }

  /**
   * If type has changed from previous paragraph then class as a new "group".
   *
   * A group is a set of the same type of paragraphs.
   */
  protected function addGroupElementContext(&$elements, $delta, $previous_bundle = '') {
    static $entity_group = 0;
    $current_bundle = $this->findElementBundle($elements, $delta);
    if ($previous_bundle != $current_bundle) {
      $entity_group++;
    }
    $elements[$delta]['#attributes']['data-entity-context-group'] = $entity_group;
  }

  /**
   * Add data attributes for the next paragraph type.
   *
   * Alternatively add last if there is no previous paragraph.
   */
  protected function addNextElementContext(&$elements, $delta) {
    if ($entity_bundle = $this->findElementBundle($elements, $delta + 1)) {
      $elements[$delta]['#attributes']['data-entity-context-next'] = $entity_bundle;
    }
    else {
      $elements[$delta]['#attributes']['data-entity-context-last'] = TRUE;
    }
  }

  /**
   * Given delta of set of elements determine the type of element (i.e. bundle).
   */
  protected function findElementBundle($elements, $delta) {
    if (isset($elements[$delta])) {
      return $elements[$delta]['#' . $this->getFieldSetting('target_type')]->bundle();
    }
    return FALSE;
  }

  /**
   * Add position (delta + 1) of element within the list and add odd/even.
   */
  protected function addPositionContext(&$elements, $delta) {
    $elements[$delta]['#attributes']['data-entity-context-position'] = $delta + 1;
    if (($delta + 1) % 2 == 1) {
      $elements[$delta]['#attributes']['data-entity-context-odd'] = TRUE;
    }
    else {
      $elements[$delta]['#attributes']['data-entity-context-even'] = TRUE;
    }
  }

}
